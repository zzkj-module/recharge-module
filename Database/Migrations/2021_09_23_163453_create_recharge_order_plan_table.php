<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeOrderPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('recharge_order_plan')) {
            Schema::create('recharge_order_plan', function (Blueprint $table) {
                $table->increments('id')->comment('主键ID');
                $table->unsignedInteger('order_id')->default(0)->index('order_id')->comment('订单ID');
                $table->unsignedInteger('plan_id')->index('plan_id')->comment('主键ID');
                $table->string('plan_name')->default('')->comment('方案名称');
                $table->decimal('money', 10)->unsigned()->default(0.00)->comment('充值金额');
                $table->decimal('gift_money', 10)->unsigned()->default(0.00)->comment('赠送金额');
                $table->decimal('gift_integral', 10)->unsigned()->default(0.00)->comment('赠送积分');
                $table->string('gift_coupons')->default('')->comment('赠送优惠券');
                $table->unsignedInteger('store_id')->default(0)->index('store_id')->comment('小程序商城ID');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_order_plan');
    }
}
