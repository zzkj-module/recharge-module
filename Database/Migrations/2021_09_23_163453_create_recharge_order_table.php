<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('recharge_order')) {
            Schema::create('recharge_order', function (Blueprint $table) {
                $table->increments('id')->comment('订单ID');
                $table->string('order_no', 20)->default('')->index('order_no')->comment('订单号');
                $table->unsignedInteger('user_id')->default(0)->index('user_id')->comment('用户ID');
                $table->unsignedTinyInteger('recharge_type')->default(10)->comment('充值方式(10自定义金额 20套餐充值)');
                $table->unsignedInteger('plan_id')->default(0)->index('plan_id')->comment('充值套餐ID');
                $table->decimal('pay_price', 10)->unsigned()->default(0.00)->comment('用户支付金额');
                $table->decimal('gift_money', 10)->unsigned()->default(0.00)->comment('赠送金额');
                $table->decimal('gift_integral', 10)->default(0.00)->comment('赠送积分');
                $table->decimal('actual_money', 10)->unsigned()->default(0.00)->comment('实际到账金额');
                $table->unsignedTinyInteger('pay_status')->default(10)->comment('支付状态(10待支付 20已支付)');
                $table->timestamp('pay_time')->nullable()->comment('付款时间');
                $table->string('transaction_id', 30)->default('')->comment('微信支付交易号');
                $table->unsignedInteger('store_id')->default(0)->index('store_id')->comment('小程序商城ID');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_order');
    }
}
