<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('recharge_plan')) {
            Schema::create('recharge_plan', function (Blueprint $table) {
                $table->increments('plan_id')->comment('主键ID');
                $table->string('plan_name')->default('')->comment('套餐名称');
                $table->decimal('money', 10)->unsigned()->default(0.00)->comment('充值金额');
                $table->decimal('gift_money', 10)->unsigned()->default(0.00)->comment('赠送金额');
                $table->decimal('gift_integral', 10)->default(0.00)->comment('赠送积分');
                $table->unsignedInteger('sort')->default(0)->comment('排序(数字越小越靠前)');
                $table->unsignedInteger('store_id')->default(0)->index('store_id')->comment('小程序商城ID');
                $table->string('gift_coupons')->default('')->comment('赠送优惠券');
                $table->string('describe')->default('')->comment('描述');
                $table->boolean('status')->default(1)->comment('状态0关闭1正常');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_plan');
    }
}
