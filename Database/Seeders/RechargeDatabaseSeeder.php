<?php

namespace Modules\Recharge\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RechargeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            if (Schema::hasTable('auth_menu')){
                $data = json_decode(file_get_contents(module_path('Recharge') . DIRECTORY_SEPARATOR . 'menu.php'), true);
                $module = 'Recharge';
                DB::table('auth_menu')->where("module", $module)->delete();
                addInfo($data, 0);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
