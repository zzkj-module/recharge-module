<?php
/**
 *BaseModel
 * @author tan bing
 * @date 2021-06-21 17:42
 */


namespace Modules\Recharge\Entities;


use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
//    use SoftDeletes;
    //可搜索字段
    protected $searchField = [];

    //可作为条件的字段
    protected $whereField = [];

    //可做为时间范围查询的字段
    protected $timeField = ['created_at'];

    //禁止删除的数据id
    public $noDeletionId = [];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format($this->dateFormat ?: 'Y-m-d H:i:s');
    }

    /**
     * 查询处理
     *
     * @param $query
     * @param $param
     */
    public function scopeAddWhere($query, $param): void
    {
        //关键词like搜索
        $keywords = $param['_keywords'] ?? '';

        if (!empty($keywords) && count($this->searchField) > 0) {
            array_walk($this->searchField,function (&$val){
                $val = '`'.$val.'`';
                return true;
            });
            $this->searchField = implode(',', $this->searchField);
            $query->whereRaw("concat(".$this->searchField.") like '%".$keywords."%'");
        }

        //字段条件查询
        if (count($this->whereField) > 0 && count($param) > 0) {
            foreach ($param as $key => $value) {
                if (!empty($value) && in_array((string)$key, $this->whereField, true)) {
                    $query->where($this->table . '.' . $key, $value);
                }
            }
        }

        //时间范围查询
        if (count($this->timeField) > 0 && count($param) > 0) {
            foreach ($param as $key => $value) {
                if (!empty($value) && in_array((string)$key, $this->timeField, true)) {
                    $time_range = explode(' - ', $value);
                    [$start_time, $end_time] = $time_range;

                    //如果是int，进行转换
//                    if(false !== strtotime($start_time)){
//                        $start_time = strtotime($start_time);
//                        if (strlen($end_time) === 10) {
//                            $end_time .= '23:59:59';
//                        }
//                        $end_time = strtotime($end_time);
//                    }
                    $query->whereBetween($this->table . '.' . $key, [$start_time, $end_time]);
                }
            }
        }

        //排序
        $order = $param['_order'] ?? '';
        $by    = $param['_by'] ?? 'desc';

        $query->orderBy($order ?: $this->table . '.' . $this->primaryKey, $by ?: 'desc');
    }

    public function getNoDeletionId(){

        return $this->noDeletionId;

    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-06-24 9:55
     */
    public function deleteData($id)
    {
        return $this->where('id', $id)->delete();
    }

    /**
     * 批量删除数据
     * @param $ids
     * @return mixed
     * @author tan bing
     * @date 2021-06-24 9:55
     */
    public function batchDeleteData($ids)
    {
        return $this->whereIn('id', $ids)->delete();
    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-06-24 9:55
     */
    public function forceDeleteData($id)
    {
        return $this->destroy($id);
    }

    /**
     * 批量删除数据
     * @param $ids
     * @return mixed
     * @author tan bing
     * @date 2021-06-24 9:55
     */
    public function batchForceDeleteData($ids)
    {
        return $this->destroy($ids);
    }

    /**
     * 编辑状态值.
     * @param $param
     * @param string $key
     * @return
     * @author tan bing
     * @date 2021-06-24 10:58
     */
    public function editStatus($param)
    {
        return $this->where($this->primaryKey, $param['id'])->update(['status' => $param['status']]);
    }
}