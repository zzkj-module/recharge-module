<?php
/**
 *Users
 * @author tan bing
 * @date 2021-08-10 14:08
 */


namespace Modules\Recharge\Entities;


class RechargeOrderPlan extends BaseModel
{

    /**
     * 与模型关联的表名
     *
     * @var string
     */
    protected $table = 'recharge_order_plan';

    /**
     * 重定义主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 指示是否自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * 不可批量赋值的属性。
     *
     * @var array
     */
    protected $guarded = [];

    //可搜索字段
    protected $searchField = ['order_no', ];

    //可作为条件的字段
    protected $whereField  = ['order_id', 'plan_id'];

    // 新增字段
    protected $appends = [];

}