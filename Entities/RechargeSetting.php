<?php
/**
 *Users
 * @author tan bing
 * @date 2021-08-10 14:08
 */


namespace Modules\Recharge\Entities;


use App\Models\Setting;
use Modules\Recharge\Enums\SettingEnum;

class RechargeSetting extends Setting
{

    /**
     * 与模型关联的表名
     *
     * @var string
     */
    protected $table = 'setting';

    /**
     * 获取默认设置项
     * @return array[]
     * @author tan bing
     * @date 2021-09-17 17:47
     */
    public static function getDefaultData()
    {
        return [
            // 用户充值设置
            SettingEnum::RECHARGE => [
                'key' => SettingEnum::RECHARGE,
                'describe' => '用户充值设置',
                'values' => [
                    'is_entrance' => 1,   // 是否允许用户充值
                    'is_custom' => 1,     // 是否允许自定义金额
                    'is_match_plan' => 1, // 自定义金额是否自动匹配合适的套餐
                    'describe' => "1. 账户充值仅限微信在线方式支付，充值金额实时到账；\n" .
                        "2. 账户充值套餐赠送的金额即时到账；\n" .
                        "3. 账户余额有效期：自充值日起至用完即止；\n" .
                        "4. 若有其它疑问，可拨打客服电话400-000-1234",     // 充值说明
                ],
            ],
        ];
    }

}