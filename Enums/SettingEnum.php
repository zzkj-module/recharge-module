<?php

namespace Modules\Recharge\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static RECHARGE()
 */
final class SettingEnum extends Enum
{
    const RECHARGE =   'recharge';

    public static function data()
    {
        return [
            self::RECHARGE => [
                'value' => self::RECHARGE,
                'describe' => '充值设置',
            ],
        ];
    }
}
