<?php
/**
 *RechargeConfigController
 * @author tan bing
 * @date 2021-09-18 10:02
 */


namespace Modules\Recharge\Http\Controllers\Admin;


use Modules\Recharge\Entities\RechargeSetting;
use Modules\Recharge\Services\Admin\RechargeConfigServices;
use Psr\SimpleCache\InvalidArgumentException;

class RechargeConfigController extends AdminBaseController
{

    private $rechargeConfigService;

    public function __construct(RechargeConfigServices $rechargeConfigService)
    {
        parent::__construct();
        $this->rechargeConfigService =  $rechargeConfigService;
    }

    /**
     * 充值设置页面
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @author tan bing
     * @date 2021-09-18 10:44
     */
    public function index()
    {
        try {
            $setting = $this->rechargeConfigService->getRechargeConfig();
        } catch (InvalidArgumentException $e) {
            $setting = [];
        }
        return view('recharge::admin.recharge_setting.index', compact('setting'));
    }

    /**
     * 编辑配置项
     * @return mixed
     * @throws InvalidArgumentException
     * @author tan bing
     * @date 2021-09-18 10:46
     */
    public function saveSetting()
    {
        try {
            $result = $this->rechargeConfigService->saveRechargeConfig();
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }

        return $this->success($result);
    }
}