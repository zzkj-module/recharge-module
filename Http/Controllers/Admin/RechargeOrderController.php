<?php
/**
 * RechargeOrderController
 * @author tan bing
 * @date 2021-09-17 11:45
 */


namespace Modules\Recharge\Http\Controllers\Admin;


use Modules\Recharge\Services\Admin\RechargeOrderServices;

class RechargeOrderController extends AdminBaseController
{

    /**
     * @var RechargeOrderServices
     * @author tan bing
     */
    private $rechargeOrderServices;

    /**
     * RechargeOrderController constructor.
     * @param RechargeOrderServices $rechargeOrderServices
     * @author tan bing
     * @date 2021-09-17 14:54
     */
    public function __construct(RechargeOrderServices $rechargeOrderServices)
    {
        parent::__construct();
        $this->rechargeOrderServices = $rechargeOrderServices;
    }

    /**
     * 列表页面
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @author tan bing
     * @date 2021-09-17 14:54
     */
    public function index()
    {
        return view('recharge::admin.recharge_order.index');
    }

    /**
     * 获取详情
     * @return mixed
     * @author tan bing
     * @date 2021-09-17 14:54
     */
    public function getDetail()
    {
        $result = $this->rechargeOrderServices->getDetail();
        return $this->success($result);
    }

    /**
     * 获取列表 --分页
     * @return mixed
     * @author tan bing
     * @date 2021-09-17 14:54
     */
    public function getPageData()
    {
        $result = $this->rechargeOrderServices->getPageData();
        return $this->success($result);
    }
}