<?php
/**
 * RechargePlanController
 * @author tan bing
 * @date 2021-09-15 17:19
 */


namespace Modules\Recharge\Http\Controllers\Admin;


use Modules\Recharge\Services\Admin\RechargePlanServices;

class RechargePlanController extends AdminBaseController
{

    /**
     * @var RechargePlanServices
     * @author tan bing
     */
    private $rechargePlanServices;

    /**
     * CouponController constructor.
     * @param RechargePlanServices $rechargePlanServices
     * @author tan bing
     * @date 2021-09-15 17:25
     */
    public function __construct(RechargePlanServices $rechargePlanServices)
    {
        parent::__construct();
        $this->rechargePlanServices = $rechargePlanServices;
    }

    /**
     * 优惠券管理页面
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @author tan bing
     * @date 2021-09-15 17:30
     */
    public function index()
    {
        $param = $this->rechargePlanServices->getRequestParam();
        return view('recharge::admin.recharge_plan.index', compact('param'));
    }

    /**
     * 优惠券编辑页面
     * @return bool|\Illuminate\Auth\Access\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @author tan bing
     * @date 2021-09-15 17:30
     */
    public function edit()
    {
        $info = $this->rechargePlanServices->getRechargeDetail();
        return view('recharge::admin.recharge_plan.edit', compact('info'));
    }

    /**
     * 获取优惠券列表
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:30
     */
    public function getPageData()
    {
        $result = $this->rechargePlanServices->getPageData();
        return $this->success($result);
    }

    /**
     * 添加/编辑优惠券
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:31
     */
    public function addAndEditDetail()
    {
        try {
            $result = $this->rechargePlanServices->addAndEditDetail();
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }

        return $this->success($result);
    }

    /**
     * 删除
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:53
     */
    public function delete()
    {
        try {
            $result = $this->rechargePlanServices->delete();
            if(!$result)
                return $this->failed('删除失败');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }

        return $this->success();
    }

    /**
     * 批量删除.
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 13:46
     */
    public function batchDelete()
    {
        try {
            $result = $this->rechargePlanServices->batchDelete();
            if(!$result)
                return $this->failed('删除失败');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }

        return $this->success();
    }

    /**
     * 编辑状态.
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 13:46
     */
    public function editStatus()
    {
        try {
            $result = $this->rechargePlanServices->editStatus();
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }

        return $this->success($result);
    }

    /**
     * 获取优惠券列表
     * @return mixed
     * @author tan bing
     * @date 2021-09-17 10:25
     */
    public function getCouponList()
    {
        $result = $this->rechargePlanServices->getCouponList();
        return $this->success($result);
    }

}