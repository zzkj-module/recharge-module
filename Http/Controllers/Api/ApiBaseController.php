<?php
/**
 *ApiBaseController
 * @author tan bing
 * @date 2021-06-10 16:59
 */


namespace Modules\Recharge\Http\Controllers\Api;


use App\Helpers\ApiResponse;
use Illuminate\Routing\Controller as BaseController;

class ApiBaseController extends BaseController
{
    use ApiResponse;
}