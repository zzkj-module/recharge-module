<?php
/**
 *RechargeController
 * @author tan bing
 * @date 2021-10-12 14:40
 */


namespace Modules\Recharge\Http\Controllers\Api;


use App\Helpers\HttpRequest;
use Modules\Recharge\Services\Api\RechargeServices;
use Modules\Wechatapplet\Enums\PayEnum;
use Nwidart\Modules\Facades\Module;

class RechargeController extends ApiBaseController
{

    /**
     * @var RechargeServices
     * @author tan bing
     */
    protected RechargeServices $rechargeService;

    /**
     * RechargeController constructor.
     *
     * @param RechargeServices $rechargeService
     * @author tan bing
     * @date 2021-10-12 15:09
     */
    public function __construct(RechargeServices $rechargeService)
    {
        $this->rechargeService = $rechargeService;
    }

    /**
     * 余额页面
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @author tan bing
     * @date 2021-10-29 9:49
     */
    public function rechargeIndex()
    {
        $data = $this->rechargeService->getRechargeOrder();
        $date = $data['date'];
        $list = $data['list'];
        $user = $this->rechargeService->user();
        $money = $user->money;
        $setting = $this->rechargeService->getRechargeConfig();
        return $this->success(compact('date', 'money', 'list', 'setting'));
    }

    public function rechargeDetail()
    {
        $result = $this->rechargeService->rechargeDetail();
        return $this->success($result);
    }

    /**
     * 获取充值设置
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @author tan bing
     * @date 2021-10-12 14:44
     */
    public function getRechargeConfig()
    {
        $result = $this->rechargeService->getRechargeConfig();
        return $this->success($result);
    }

    /**
     * 获取充值方案
     * @return mixed
     * @author tan bing
     * @date 2021-10-12 14:44
     */
    public function getRechargePlan()
    {
        $result = $this->rechargeService->getRechargePlan();
        return $this->success($result);
    }

    /**
     * 获取充值订单
     * @return mixed
     * @author tan bing
     * @date 2021-10-12 15:28
     */
    public function getRechargeOrder()
    {
        $result = $this->rechargeService->getRechargeOrder();
        return $this->success($result);
    }

    /**
     * 充值余额.
     * @return mixed
     * @throws \Throwable
     * @author tan bing
     * @date 2021-10-15 9:44
     */
    public function recharge()
    {
        try {
            $result = $this->rechargeService->rechargeOrder();
            $postData = [
                'user_id' => $result->user_id,
                'body' => '充值余额',
                'order_no' => $result->order_no . time(),
                'price' => $result->pay_price,
                'notify_url' => url('/api/pay/notify'),
                'token' => config('module-server.token'),
                'paid_type' => PayEnum::RECHARGE,
            ];
            $response = HttpRequest::sendRequest(url('/api/pay/wechat-pay'), $postData, 'POST');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
        return $this->success($response);
    }
}