<?php


namespace Modules\Recharge\Repositories\Contracts;


interface RechargeConfigInterface
{

    /**
     * 根据ID获取信息.
     *
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 13:08
     */
    public function findById($id);

}