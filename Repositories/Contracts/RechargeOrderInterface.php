<?php


namespace Modules\Recharge\Repositories\Contracts;


interface RechargeOrderInterface
{

    /**
     * @param $param
     * @param $perPage
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 12:59
     */
    public function getPageData($param, $perPage);

    public function get($param);

    /**
     * 根据ID获取信息.
     *
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 13:08
     */
    public function findById($id);

    /**
     * @param $data
     * @return mixed
     * @author tan bing
     * @date 2021-10-15 9:30
     */
    public function addRechargeOrder($data);

    /**
     * @param $data
     * @return mixed
     * @author tan bing
     * @date 2021-10-14 18:08
     */
    public function addRechargeOrderPlan($data);

}