<?php


namespace Modules\Recharge\Repositories\Contracts;


interface RechargePlanInterface
{

    /**
     * 获取用户信息分页数据.
     *
     * @param $param
     * @param $perPage
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 12:59
     */
    public function getPageData($param, $perPage);

    /**
     * @param $param
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:42
     */
    public function getList($param);

    /**
     *
     * @param $param
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 12:59
     */
    public function addAndEditDetail($param);

    /**
     * 根据ID获取信息.
     *
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-07-19 13:08
     */
    public function findById($id);

}