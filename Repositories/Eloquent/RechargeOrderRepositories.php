<?php
/**
 *CustomerRepositories
 * @author tan bing
 * @date 2021-07-19 12:59
 */


namespace Modules\Recharge\Repositories\Eloquent;


use App\Extend\Random;
use Modules\Recharge\Entities\RechargeOrder;
use Modules\Recharge\Entities\RechargeOrderPlan;
use Modules\Recharge\Repositories\Contracts\RechargeOrderInterface;

class RechargeOrderRepositories implements RechargeOrderInterface
{

    /**
     * @param $id
     * @return mixed
     * @author tan bing
     * @date 2021-10-15 9:37
     */
    public function findById($id)
    {
        return RechargeOrder::find($id);
    }

    /**
     * @param $param
     * @param $perPage
     * @return mixed
     * @author tan bing
     * @date 2021-10-15 9:37
     */
    public function getPageData($param, $perPage)
    {
        return RechargeOrder::addWhere($param)
            ->leftJoin('Users', 'users.id', '=', 'recharge_order.user_id')
            ->leftJoin('recharge_order_plan', 'recharge_order_plan.order_id', '=', 'recharge_order.id')
            ->select('recharge_order.*', 'recharge_order_plan.plan_name', 'users.nickname', 'users.id', 'users.avatar')
            ->paginate(!empty($param['limit']) ? $param['limit'] : $perPage);
    }

    public function get($param)
    {
        return RechargeOrder::addWhere($param)
            ->leftJoin('Users', 'users.id', '=', 'recharge_order.user_id')
            ->leftJoin('recharge_order_plan', 'recharge_order_plan.order_id', '=', 'recharge_order.id')
            ->select('recharge_order.*', 'recharge_order_plan.plan_name', 'users.nickname', 'users.id', 'users.avatar')
            ->get();
    }

    /**
     * 创建充值订单
     * @param $data
     * @return mixed|RechargeOrder
     * @throws \Exception
     * @author tan bing
     * @date 2021-10-15 9:32
     */
    public function addRechargeOrder($data)
    {
        $recharge                = new RechargeOrder();
        $recharge->pay_status    = 10;
        $recharge->user_id       = $data['user_id'];
        $recharge->plan_id       = $data['plan_id'];
        $recharge->pay_price     = $data['amount'];
        $recharge->gift_money    = $data['gift_amount'];
        $recharge->recharge_type = $data['recharge_type'];
        $recharge->gift_integral = $data['gift_integral'];
        $recharge->actual_money  = $data['amount'] + $data['gift_amount'];
        $recharge->order_no      = $data['order_sn'];
        if (!$recharge->save())
            throw new \Exception('充值失败【创建充值订单失败】');

        return $recharge;
    }

    /**
     *
     * @param $data
     * @return RechargeOrderPlan
     * @throws \Exception
     * @author tan bing
     * @date 2021-10-14 18:08
     */
    public function addRechargeOrderPlan($data)
    {
        $rechargeOrderPlan                = new RechargeOrderPlan();
        $rechargeOrderPlan->order_id      = $data['order_id'];
        $rechargeOrderPlan->plan_id       = $data['plan_id'];
        $rechargeOrderPlan->gift_money    = $data['gift_amount'];
        $rechargeOrderPlan->gift_integral = $data['gift_integral'];
        $data['gift_coupons'] && $rechargeOrderPlan->gift_coupons  = $data['gift_coupons'];
        $data['plan_name'] && $rechargeOrderPlan->plan_name     = $data['plan_name'];
        if (!$rechargeOrderPlan->save())
            throw new \Exception('充值失败【创建充值订单失败】');

        return $rechargeOrderPlan;
    }

}