<?php
/**
 *CustomerRepositories
 * @author tan bing
 * @date 2021-07-19 12:59
 */


namespace Modules\Recharge\Repositories\Eloquent;


use Modules\Recharge\Entities\RechargePlan;
use Modules\Recharge\Repositories\Contracts\RechargePlanInterface;

class RechargePlanRepositories implements RechargePlanInterface
{

    public function findById($id)
    {
        return RechargePlan::find($id);
    }

    public function getPageData($param, $perPage)
    {
        return RechargePlan::addWhere($param)->paginate(!empty($param['limit']) ? $param['limit'] : $perPage);
    }

    public function getList($param)
    {
        return RechargePlan::addWhere($param)->get();
    }

    public function addAndEditDetail($data)
    {
        if (!empty($data['id'])) {
            $rechargePlanModel = RechargePlan::find($data['id']);
            if (!$rechargePlanModel) {
                $rechargePlanModel = new RechargePlan();
            }
        } else {
            $rechargePlanModel = new RechargePlan();
        }

        $rechargePlanModel->sort          = $data['sort'];
        $rechargePlanModel->money         = $data['money'];
        $rechargePlanModel->describe      = $data['describe'];
        $rechargePlanModel->plan_name     = $data['plan_name'];
        $rechargePlanModel->gift_money    = $data['gift_money'];
        $rechargePlanModel->gift_coupons  = $data['gift_coupons'];
        $rechargePlanModel->gift_integral = $data['gift_integral'];

        if (!$rechargePlanModel->save()) throw new \Exception('添加或编辑失败，请重试');

        return $rechargePlanModel;
    }
}