@extends('admin.public.header')
@section('title','充值订单管理')

@section('listsearch')
    <fieldset class="table-search-fieldset">
        <legend>搜索信息</legend>
        <div style="margin: 10px 10px 10px 10px">
            <form class="layui-form layui-form-pane form-search" action="" id="searchFrom">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">充值订单号</label>
                        <div class="layui-input-inline">
                            <input type="text" name="_keywords" id="_keywords" autocomplete="off" value="" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">充值方式</label>
                        <div class="layui-input-inline">
                            <select name="recharge_type" id="recharge_type" lay-search>
                                <option value="">全部</option>
                                <option value="10">自定义金额</option>
                                <option value="20">套餐充值</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">支付状态</label>
                        <div class="layui-input-inline">
                            <select name="pay_status" id="pay_status" lay-search>
                                <option value="">全部</option>
                                <option value="10">待支付</option>
                                <option value="20">已支付</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">付款时间</label>
                        <div class="layui-input-inline">
                            <input type="text" name="begin_time" id="begin_time" placeholder="请选择起始时间" autocomplete="off" value="" class="layui-input">
                        </div>
                        <div class="layui-input-inline">
                            <input type="text" name="end_time" id="end_time" placeholder="请选择截止时间" autocomplete="off" value="" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button type="submit" class="layui-btn layui-btn-sm layui-btn-normal"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        <button type="button" class="layui-btn layui-btn-sm layui-btn-primary data-reset-btn" lay-submit lay-filter="data-reset-btn" >重置 </button>
                    </div>
                </div>
            </form>
        </div>
    </fieldset>
@endsection

@section('listcontent')
<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
<!-- 表头左侧按钮 -->
<script type="text/html" id="toolbarColumn">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm layuimini-btn-primary" onclick="window.location.reload();" ><i class="layui-icon layui-icon-refresh-3"></i></button>
    </div>
</script>
<!-- 操作按钮 -->
<script type="text/html" id="barOperate">

</script>
@endsection

@section('listscript')
<script type="text/javascript">
    layui.use(['form','table', 'dropdown'], function(){
        var table = layui.table,
            $=layui.jquery,
            form = layui.form,
            dropdown = layui.dropdown;

        // 渲染表格
        var loading = layer.msg('加载中..', {icon: 16,shade: 0.3,time: false});
        table.render({
                elem: '#tableList',
                url:'/admin/recharge-order/page-data',
                parseData: function(res) { //res 即为原始返回的数据
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.message, //解析提示文本
                        "count": res.data.count, //解析数据长度
                        "data": res.data.data //解析数据列表
                    }
                },
                cellMinWidth: 80,//全局定义常规单元格的最小宽度
                toolbar: '#toolbarColumn',//开启头部工具栏，并为其绑定左侧模板
                defaultToolbar: ['filter', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                    title: '搜索',
                    layEvent: 'TABLE_SEARCH',
                    icon: 'layui-icon-search'
                }],
            title: '充值套餐列表',
            cols: [[
                {type: 'checkbox', align: 'center'},
                {field:'id', title:'ID', width:100, align: 'center', unresize: true, sort: true},
                {title:'会员信息', width:180,  align: 'center', templet: function(res){
                    return res.nickname + '<img style="width:50px;" title="点击查看" onclick="showBigImage(this)" src="'+res.avatar+'">'
                }},
                {field:'order_no', title:'订单号', width:200, align: 'center', unresize: true, sort: true},

                {field:'recharge_type_text', title: '充值方式', width:120,  align: 'center'},
                {title: '套餐名称',  align: 'center', templet: function(res){
                    if(res.plan_name != null) {
                        return res.plan_name;
                    } else {
                        return '自定义金额';
                    }
                }},
                {title: '充值金额（元）', width:150,  align: 'center', templet: function(res){
                    return '<span style="color:#1E9FFF">￥' + res.pay_price + '</span>'
                }},
                {title: '赠送金额（元）', width:130,  align: 'center', templet: function(res){
                    return '<span style="color:#1E9FFF">￥' + res.gift_money + '</span>'
                }},
                {title: '赠送积分', width:130,  align: 'center', templet: function(res){
                    return '<span style="color:#1E9FFF">' + res.gift_integral + '</span>'
                }},

                {field: 'pay_status_text', title:'支付状态', width:100, align: 'center'},

                {field:'pay_time', title:'付款时间', width:160, align: 'center'},
                {field:'created_at', title:'创建时间', width:160, align: 'center'},
            ]],
            id: 'listReload',
            limits: [15, 20, 30, 50, 100,200],
            limit: 15,
            page: true,
            done: function () {
                layer.close(loading);
            },
            text: {
                none: '抱歉！暂无数据~' //默认：无数据。注：该属性为 layui 2.2.5 开始新增
            }
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('listReload', {
                where: {
                    _keywords: $("#_keywords").val(),
                    recharge_type: $("#recharge_type").val(),
                    pay_status: $("#pay_status").val(),
                    pay_time: $("#begin_time").val() + ' - ' + $("#end_time").val(),
                }
            });
            return false;
        });

        // 监听重置操作
        form.on('submit(data-reset-btn)', function (data) {
            $("#_keywords").val('');
            $("#begin_time").val('');
            $("#end_time").val('');
        });

    });
    
</script>
@endsection
