@extends('admin.public.header')
@section('title','添加/编辑优惠券')

@section('listcontent')
    <div class="layui-tab-content">
        <form class="layui-form">
            <div class="layui-form layuimini-form">
                <input type="hidden" name="id" value="{{ $info->plan_id ?? '' }}">
                <div class="layui-form-item">
                    <label class="layui-form-label required">套餐名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="plan_name" lay-verify="required" lay-reqtext="套餐名称不能为空" placeholder="请输入套餐名称" value="{{ $info->plan_name ?? '' }}" class="layui-input">
                        <tip>便于后台查找，例如充100元送100元</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label required">充值金额</label>
                    <div class="layui-input-block">
                        <input type="number" name="money" placeholder="金额不能小于0，可保留两位小数" value="{{$info->money ?? 0 }}" class="layui-input">
                        <tip>金额不能小于0，可保留两位小数</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label required">赠送金额</label>
                    <div class="layui-input-block">
                        <input type="number" name="gift_money" placeholder="请输入赠送金额，为0表不赠送" value="{{$info->gift_money ?? 0 }}" class="layui-input ">
                        <tip>请输入赠送金额，为0表不赠送</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label required">赠送积分</label>
                    <div class="layui-input-block">
                        <input type="number" name="gift_integral" placeholder="请输入赠送积分，为0表不赠送" value="{{$info->gift_integral ?? 0 }}" class="layui-input">
                        <tip>请输入赠送积分，为0表不赠送</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">赠送优惠券</label>
                    <div class="layui-input-block">
                        <div id="gift_coupons"></div>
                        <tip>选择需要赠送的优惠券，不选择即不赠送</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label required">排序</label>
                    <div class="layui-input-block">
                        <input type="number" name="sort" value="{{$info->sort ?? 100 }}" class="layui-input">
                        <tip>数字越小越靠前</tip>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">套餐描述</label>
                    <div class="layui-input-block">
                        <textarea name="describe" placeholder="请输入套餐描述" class="layui-textarea">{{ $info->describe ?? '' }}</textarea>
                    </div>
                </div>
            </div>
            <div class="hr-line"></div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal" id="saveBtn" lay-submit lay-filter="saveBtn">确认保存</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('listscript')
    <script type="text/javascript">
        layui.use(['layer','form','xmSelect'], function () {
            var layer = layui.layer,
                form = layui.form,
                $ = layui.jquery ,
                xmSelect = layui.xmSelect;

            form.on('submit(saveBtn)', function(data){
                var loading = layer.msg('加载中..', {icon: 16,shade: 0.3,time: false});
                $.ajax({
                    url:'/admin/recharge-plan/edit-detail',
                    type:'post',
                    data: data.field,
                    dataType:"JSON",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(res){
                        layer.close(loading);  //返回数据关闭loading
                        if(res.code == 0){
                            layer.msg(res.message, {icon: 1},function(){
                                setTimeout('parent.location.reload()',500);
                            });
                        }
                        else{
                            layer.msg(res.message,{icon: 2});
                        }
                    },
                    error:function(e){
                        layer.close(loading);
                        layer.msg("提交失败",{icon: 2});
                    },
                });
                return false;
            });

            //渲染多选
            var id = $('input[name="id"]').val();
            $.ajax({
                url:'/admin/recharge-plan/get-coupon-list',
                type:'get',
                data:{'id':id},
                dataType:'JSON',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (res) {
                    if (res.code == 0) {
                        xmSelect.render({
                            el: '#gift_coupons',
                            layVerify: '',
                            name: 'gift_coupons',
                            toolbar: {
                                show: true,
                            },
                            filterable: true,
                            theme: {
                                color: '#a5673f',
                            },
                            data: res.data
                        })
                    } else {
                        layer.msg(res.message, {icon: 2});
                    }
                },
                error: function (err) {
                    layer.msg(err.message, {icon: 2});
                }
            });

        });
    </script>
@endsection

