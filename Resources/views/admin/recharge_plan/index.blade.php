@extends('admin.public.header')
@section('title','充值套餐管理')

@section('listsearch')
    <fieldset class="table-search-fieldset">
        <legend>搜索信息</legend>
        <div style="margin: 10px 10px 10px 10px">
            <form class="layui-form layui-form-pane form-search" action="" id="searchFrom">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">套餐名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="_keywords" id="recharge_plan_name" autocomplete="off" value="" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button type="submit" class="layui-btn layui-btn-sm layui-btn-normal"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        <button type="button" class="layui-btn layui-btn-sm layui-btn-primary data-reset-btn" lay-submit lay-filter="data-reset-btn" >重置 </button>
                    </div>
                </div>
            </form>
        </div>
    </fieldset>
@endsection

@section('listcontent')
<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
<!-- 表头左侧按钮 -->
<script type="text/html" id="toolbarColumn">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm layuimini-btn-primary" onclick="window.location.reload();" ><i class="layui-icon layui-icon-refresh-3"></i></button>
        <button class="layui-btn layui-btn-normal layui-btn-sm" lay-event="add"><i class="layui-icon layui-icon-add-circle"></i>新增</button>
    </div>
</script>
<!-- 操作按钮 -->
<script type="text/html" id="barOperate">
    <a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    <a class="layui-btn layui-bg-red layui-btn-xs" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>删除</a>
</script>
@endsection

@section('listscript')
<script type="text/javascript">
    layui.use(['form','table', 'dropdown'], function(){
        var table = layui.table,
            $=layui.jquery,
            form = layui.form,
            dropdown = layui.dropdown;

        // 渲染表格
        var loading = layer.msg('加载中..', {icon: 16,shade: 0.3,time: false});
        table.render({
                elem: '#tableList',
                url:'/admin/recharge-plan/page-data',
                parseData: function(res) { //res 即为原始返回的数据
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.message, //解析提示文本
                        "count": res.data.count, //解析数据长度
                        "data": res.data.data //解析数据列表
                    }
                },
                cellMinWidth: 80,//全局定义常规单元格的最小宽度
                toolbar: '#toolbarColumn',//开启头部工具栏，并为其绑定左侧模板
                defaultToolbar: ['filter', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                    title: '搜索',
                    layEvent: 'TABLE_SEARCH',
                    icon: 'layui-icon-search'
                }],
            title: '充值套餐列表',
            cols: [[
                {type: 'checkbox', align: 'center'},
                {field:'plan_id', title:'套餐ID', width:100, align: 'center', unresize: true, sort: true},
                {field:'plan_name', title:'套餐名称', width:180,  align: 'center'},
                {title: '充值金额（元）', width:150,  align: 'center', templet: function(res){
                    return '<span style="color:#1E9FFF">'+res.money+'</span>'
                }},
                {title: '赠送金额（元）', width:150,  align: 'center', templet: function(res){
                        return '<span style="color:#1E9FFF">￥' + res.gift_money + '</span>'
                }},
                {title: '赠送积分', width:130,  align: 'center', templet: function(res){
                        return '<span style="color:#1E9FFF">￥' + res.gift_integral + '</span>'
                }},
                {field:'status', title:'状态', width:100, align: 'center', templet: function(res){
                    if(res.status == 1){
                        return '<input type="checkbox" name="status" value="'+res.plan_id+'" lay-skin="switch" lay-text="显示|隐藏" lay-filter="isOpen" checked>'
                    }else{
                        return '<input type="checkbox" name="status" value="'+res.plan_id+'" lay-skin="switch" lay-text="显示|隐藏" lay-filter="isOpen">'
                    }
                }},
                {field:'created_at', title:'添加时间', width:160, align: 'center'},
                {field:'describe', title:'套餐描述', align: 'center'},
                {title:'操作',toolbar: '#barOperate', align: 'center'}
            ]],
            id: 'listReload',
            limits: [15, 20, 30, 50, 100,200],
            limit: 15,
            page: true,
            done: function () {
                layer.close(loading);
            },
            text: {
                none: '抱歉！暂无数据~' //默认：无数据。注：该属性为 layui 2.2.5 开始新增
            }
        });

        //头工具栏事件
        table.on('toolbar(tableList)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            var ids = [];
            var data = checkStatus.data;
            for (var i=0;i<data.length;i++){
                ids.push(data[i].id);
            }
            switch(obj.event){
                case 'batch_delete':
                    if(!ids.length){
                        return layer.msg('请勾选要删除的数据',{icon: 2});
                    }
                    layer.confirm('确定删除选中的数据吗？', {
                    title : "操作确认",
                    skin: 'layui-layer-lan'
                    },function(index){
                        $.ajax({
                            url:'/admin/recharge-plan/batch-delete',
                            type:'post',
                            data:{'id':ids},
                            dataType:"JSON",
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success:function(data){
                                if(data.code == 0){
                                    layer.msg(data.message,{icon: 1,time:1500},function(){
                                        setTimeout('window.location.reload()',500);
                                    });
                                }
                                else{
                                    layer.msg(data.message,{icon: 2});
                                }
                            },
                            error:function(e){
                                layer.msg(e.message,{icon: 2});
                            },

                        });
                    });
                break;

                case 'add':
                    layer.open({
                        title: '添加充值套餐',
                        type: 2,
                        shade: 0.2,
                        maxmin:true,
                        shadeClose: true,
                        area: ['95%', '95%'],
                        skin: 'layui-layer-lan',
                        content: '/admin/recharge-plan/edit',
                    });
                break;
                
                //自定义头工具栏右侧图标 - 提示
                case 'TABLE_SEARCH':
                    var display = $(".table-search-fieldset").css("display"); //获取标签的display属性
                    if(display == 'none'){
                        $(".table-search-fieldset").show();
                    }else{
                        $(".table-search-fieldset").hide();
                    }
                break;
               
            };
        });

        //监听状态操作
        form.on('switch(isOpen)', function(obj){
            var checked = obj.elem.checked;
            var id = obj.value;
            checked = checked == true ? 1 : 0;
            $.ajax({
                url:'/admin/recharge-plan/edit-status',
                type:'post',
                data:{'status':checked,'id':id},
                dataType:"JSON",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success:function(data){
                    if(data.code == 0){
                        layer.msg(data.message,{icon: 1,time:1500});
                    }
                    else{
                        layer.msg(data.message,{icon: 2});
                    }
                }
                
            });
        });

        //监听行工具事件
        table.on('tool(tableList)', function(obj){
            var data = obj.data;
            var id = data.plan_id;
            if(obj.event === 'delete'){
                layer.confirm('确定删除这条数据吗？', {
                    title : "操作确认",
                    skin: 'layui-layer-lan'
                },function(index){
                    $.ajax({
                        url:'/admin/recharge-plan/delete',
                        type:'delete',
                        data:{'id':id},
                        dataType:"JSON",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success:function(data){
                            if(data.code == 0){
                                layer.msg(data.message,{icon: 1,time:1500},function(){
                                    setTimeout('window.location.reload()',200);
                                });
                            }
                            else{
                                layer.msg(data.message,{icon: 2});
                            }
                        },
                        error:function(e){
                            layer.msg(data.message,{icon: 2});
                        },
                        
                    });
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                layer.open({
                    title: '编辑充值方案',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['95%', '95%'],
                    skin: 'layui-layer-lan',
                    content: '/admin/recharge-plan/edit?id='+id,
                });
            }
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('listReload', {
                where: {
                    _keywords: $("#coupon_name").val(),
                }
            });
            return false;
        });

        // 监听重置操作
        form.on('submit(data-reset-btn)', function (data) {
            $("#_keywords").val('');
        });

    });
    
</script>
@endsection
