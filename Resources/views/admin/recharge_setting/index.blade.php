@extends('admin.public.header')
@section('title','充值设置')

@section('listcontent')
<style>
.layuimini-upload-show .uploads-delete-tip {
    position: absolute;
    right: 10px;
    font-size: 12px;
}
.layuimini-upload-show li {
    position: relative;
    display: inline-block;
    padding: 5px 0 5px 0;
        padding-right: 0px;
        padding-left: 0px;
    padding-left: 10px;
    padding-right: 10px;
    border: 1px solid #e2e2e2;
}
.badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 11px;
    font-weight: bold;
    color: #fff;
    line-height: 1;
    vertical-align: middle;
    white-space: nowrap;
    text-align: center;
    background-color: #777777;
    border-radius: 10px;
}
.passport_bg_badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 11px;
    font-weight: bold;
    color: #fff;
    line-height: 1;
    vertical-align: middle;
    white-space: nowrap;
    text-align: center;
    background-color: #777777;
    border-radius: 10px;
}
.bg-red {
    background-color: #e74c3c !important;
}
</style>
<div class="layui-tab layui-tab-brief">
    <ul class="layui-tab-title">
        <li class="layui-this">充值设置</li>
    </ul>
    <div class="layui-tab-content">
        <!--基本设置-->
        <div class="layui-tab-item layui-show">
            <form class="layui-form">
                <div class="layui-form layuimini-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label required">会员充值</label>
                        <div class="layui-input-block">
                            <input type="radio" name="is_entrance" value="1" title="开启" @if($setting['is_entrance'] ==  1)checked @endif>
                            <input type="radio" name="is_entrance" value="0" title="关闭" @if($setting['is_entrance'] ==  0)checked @endif>
                        </div>
                        <tip>如设置关闭则用户端不显示充值按钮</tip>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label required">自定义金额</label>
                        <div class="layui-input-block">
                            <input type="radio" name="is_custom" value="1" title="允许" @if($setting['is_custom'] ==  1)checked @endif>
                            <input type="radio" name="is_custom" value="0" title="不允许" @if($setting['is_custom'] ==  0)checked @endif>
                        </div>
                        <tip>是否允许用户填写自定义的充值金额</tip>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label required">自动匹配套餐</label>
                        <div class="layui-input-block">
                            <input type="radio" name="is_match_plan" value="1" title="开启" @if($setting['is_match_plan'] ==  1)checked @endif>
                            <input type="radio" name="is_match_plan" value="0" title="关闭" @if($setting['is_match_plan'] ==  0)checked @endif>
                        </div>
                        <tip>充值自定义金额时 是否自动匹配充值套餐，如不开启则不参与套餐金额赠送</tip>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label required">背景图片</label>
                        <div class="layui-input-block">
                            <input name="bg_image" lay-verify="required" placeholder="请上传背景图片" value="{{$setting['bg_image'] ?? ''}}" class="layui-input layui-col-xs6" lay-reqtext="请上传背景图片">
                            <button style="display: inline-block;position: absolute;right: 0px;" type="button" class="layui-btn" id="bg_image"><i class="layui-icon"></i>上传背景图片</button>
                        </div>
                        <ul id="bing-head_img" class="layui-input-block layuimini-upload-show" style="margin-top: 10px;">
                            <li><a><img style="width: 80px;" title="点击查看" onclick="showBigImage(this)" id="preview_bg_image" src="{{$setting['thumb_bg_image'] ?? ''}}" ></a><small class="uploads-delete-tip bg-red bg_image badge" data-upload-sign="|">×</small></li>
                        </ul>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label required">广告图片</label>
                        <div class="layui-input-block">
                            <input name="ad_image" lay-verify="required" placeholder="请上传广告图片" value="{{$setting['ad_image'] ?? ''}}" class="layui-input layui-col-xs6" lay-reqtext="请上传广告图片">
                            <button style="display: inline-block;position: absolute;right: 0px;" type="button" class="layui-btn" id="ad_image"><i class="layui-icon"></i>上传广告图片</button>
                        </div>
                        <ul id="bing-head_img" class="layui-input-block layuimini-upload-show" style="margin-top: 10px;">
                            <li><a><img style="width: 80px;" title="点击查看" onclick="showBigImage(this)" id="preview_ad_image" src="{{$setting['thumb_ad_image'] ?? ''}}" ></a><small class="uploads-delete-tip bg-red ad_image badge" data-upload-sign="|">×</small></li>
                        </ul>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">跳转链接</label>
                        <div class="layui-input-block">
                            <input name="page_url" placeholder="请输入广告图片跳转链接" class="layui-input" value="{{ $setting['page_url'] ?? '' }}">
                            <tip>广告图片点击后跳转链接</tip>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label required">充值图标</label>
                        <div class="layui-input-block">
                            <input name="recharge_describe_icon" lay-verify="required" placeholder="请上传充值说明图标" value="{{$setting['recharge_describe_icon'] ?? ''}}" class="layui-input layui-col-xs6" lay-reqtext="请上传充值说明图标">
                            <button style="display: inline-block;position: absolute;right: 0px;" type="button" class="layui-btn" id="recharge_describe_icon"><i class="layui-icon"></i>上传充值图标</button>
                        </div>
                        <ul id="bing-head_img" class="layui-input-block layuimini-upload-show" style="margin-top: 10px;">
                            <li><a><img style="width: 80px;" title="点击查看" onclick="showBigImage(this)" id="preview_recharge_describe_icon" src="{{$setting['thumb_recharge_describe_icon'] ?? ''}}" ></a><small class="uploads-delete-tip bg-red recharge_icon badge" data-upload-sign="|">×</small></li>
                        </ul>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">充值说明</label>
                        <div class="layui-input-block">
                            <textarea name="describe" placeholder="请输入充值说明" class="layui-textarea">{{ $setting['describe'] ?? '' }}</textarea>
                        </div>
                    </div>
                </div> 
                <div class="hr-line"></div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal" id="setting" lay-submit lay-filter="setting">确认保存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('listscript')
    <script type="text/javascript">
        layui.use(['layer','form','upload'], function () {
            var layer = layui.layer, form = layui.form, $=layui.jquery ,upload = layui.upload;
            //站点设置
            form.on('submit(setting)', function(data){
                var index = layer.load(0); //添加laoding,0-2两种方式
                $.ajax({
                    url:'/admin/recharge-config/edit-config',
                    type:'post',
                    data: {key: 'recharge', form: data.field},
                    dataType:"JSON",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(res){
                        layer.close(index);  //返回数据关闭loading
                        if(res.code == 0){
                            $("#setting").removeClass("layui-btn-normal").addClass("layui-btn-disabled");
                            $("#setting").attr("disabled","true");
                            layer.msg(res.message,{icon: 1},function(){
                                setTimeout('location.reload()',500);
                            });
                            
                        }
                        else{
                            layer.msg(res.message,{icon: 2});
                        }
                    },
                    error:function(e){
                        layer.close(index);  //返回数据关闭loading
                        layer.msg("提交失败",{icon: 2});
                    },
                    
                });
                return false;
            });

            var uploadBgImage = upload.render({
                elem: '#bg_image'
                ,url: '/admin/upload/upload' //改成您自己的上传接口
                ,accept: 'images' //图片
                ,acceptMime:'image/*'
                ,headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                ,before: function(obj){
                    //预读本地文件示例，不支持ie8
                    obj.preview(function(index, file, result){
                        $('#preview_bg_image').attr('src', result); //图片链接（base64）
                    });
                }
                ,done: function(res){
                    //如果上传失败
                    if(res.code > 0){
                        return layer.msg('上传失败',{icon: 2});
                    }
                    //上传成功
                    $("input[name=bg_image]").val(res.data);
                    return layer.msg('上传成功',{icon: 1});
                }
                ,error: function(){
                    //演示失败状态，并实现重传
                    var demoText = $('#demoText');
                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                    demoText.find('.demo-reload').on('click', function(){
                        uploadBgImage.upload();
                    });
                }
            });
            var uploadAdImage = upload.render({
                elem: '#ad_image'
                ,url: '/admin/upload/upload' //改成您自己的上传接口
                ,accept: 'images' //图片
                ,acceptMime:'image/*'
                ,headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                ,before: function(obj){
                    //预读本地文件示例，不支持ie8
                    obj.preview(function(index, file, result){
                        $('#preview_ad_image').attr('src', result); //图片链接（base64）
                    });
                }
                ,done: function(res){
                    //如果上传失败
                    if(res.code > 0){
                        return layer.msg('上传失败',{icon: 2});
                    }
                    //上传成功
                    $("input[name=ad_image]").val(res.data);
                    return layer.msg('上传成功',{icon: 1});
                }
                ,error: function(){
                    //演示失败状态，并实现重传
                    var demoText = $('#demoText');
                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                    demoText.find('.demo-reload').on('click', function(){
                        uploadAdImage.upload();
                    });
                }
            });
            var uploadRechargeIcon = upload.render({
                elem: '#recharge_describe_icon'
                ,url: '/admin/upload/upload' //改成您自己的上传接口
                ,accept: 'images' //图片
                ,acceptMime:'image/*'
                ,headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                ,before: function(obj){
                    //预读本地文件示例，不支持ie8
                    obj.preview(function(index, file, result){
                        $('#preview_recharge_describe_icon').attr('src', result); //图片链接（base64）
                    });
                }
                ,done: function(res){
                    //如果上传失败
                    if(res.code > 0){
                        return layer.msg('上传失败',{icon: 2});
                    }
                    //上传成功
                    $("input[name=recharge_describe_icon]").val(res.data);
                    return layer.msg('上传成功',{icon: 1});
                }
                ,error: function(){
                    //演示失败状态，并实现重传
                    var demoText = $('#demoText');
                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                    demoText.find('.demo-reload').on('click', function(){
                        uploadAdImage.upload();
                    });
                }
            });

            // 删除bg图片
            $(document).on("click", ".bg_image", function(event){
                layer.confirm('确认删除？', {
                    title : "操作确认",
                    skin: 'layui-layer-molv',
                    btn: ['确认','取消'] //按钮
                }, function(){
                    $("#preview_bg_image").attr('src','');
                    $("input[name=bg_image]").val('');
                    layer.msg("操作成功",{icon: 1});
                }, function(){
                    layer.msg('已取消', {icon: 1});
                });
            });

            // 删除ad图片
            $(document).on("click", ".ad_image", function(event){
                layer.confirm('确认删除？', {
                    title : "操作确认",
                    skin: 'layui-layer-molv',
                    btn: ['确认','取消'] //按钮
                }, function(){
                    $("#preview_ad_image").attr('src','');
                    $("input[name=ad_image]").val('');
                    layer.msg("操作成功",{icon: 1});
                }, function(){
                    layer.msg('已取消', {icon: 1});
                });
            });

            // 删除ad图片
            $(document).on("click", ".recharge_icon", function(event){
                layer.confirm('确认删除？', {
                    title : "操作确认",
                    skin: 'layui-layer-molv',
                    btn: ['确认','取消'] //按钮
                }, function(){
                    $("#preview_recharge_describe_icon").attr('src','');
                    $("input[name=recharge_describe_icon]").val('');
                    layer.msg("操作成功",{icon: 1});
                }, function(){
                    layer.msg('已取消', {icon: 1});
                });
            });
        });
    </script>
@endsection

