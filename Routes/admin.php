<?php

use Illuminate\Support\Facades\Route;

//=============================================================充值套餐方案管理相关接口=======================================================================//
Route::get('recharge-plan/index', 'RechargePlanController@index');     // 列表页
Route::get('recharge-plan/edit', 'RechargePlanController@edit');     // 编辑页面
Route::get('recharge-plan/page-data', 'RechargePlanController@getPageData');     // 获取分页列表数据
Route::get('recharge-plan/get-list', 'RechargePlanController@getPageData');     // 获取所有数据
Route::delete('recharge-plan/delete', 'RechargePlanController@delete');     // 删除
Route::delete('recharge-plan/batch-delete', 'RechargePlanController@batchDelete');     // 批量删除
Route::post('recharge-plan/edit-detail', 'RechargePlanController@addAndEditDetail');     // 编辑
Route::post('recharge-plan/edit-status', 'RechargePlanController@editStatus');     // 修改状态

Route::get('recharge-plan/get-coupon-list', 'RechargePlanController@getCouponList');     // 获取优惠券列表

//=============================================================充值订单管理相关接口=======================================================================//
Route::get('recharge-order/index', 'RechargeOrderController@index');               // 列表页
Route::get('recharge-order/get-detail', 'RechargeOrderController@getDetail');      // 获取详情
Route::get('recharge-order/page-data', 'RechargeOrderController@getPageData');     // 获取分页列表数据

//=============================================================充值设置相关接口=======================================================================//
Route::get('recharge-config/index', 'RechargeConfigController@index');
Route::post('recharge-config/edit-config', 'RechargeConfigController@saveSetting');
