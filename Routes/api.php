<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function() {

    Route::get('recharge/recharge-plan', 'RechargeController@getRechargePlan');         // 获取充值方案
    Route::middleware('auth.api')->group(function () {

        Route::get('recharge/recharge-config', 'RechargeController@getRechargeConfig');     // 获取充值设置
        Route::get('recharge/recharge-order', 'RechargeController@getRechargeOrder');       // 获取充值订单
        Route::get('recharge/index', 'RechargeController@rechargeIndex');                   // 充值首页
        Route::post('recharge/create-recharge', 'RechargeController@recharge');              // 创建充值订单
        Route::get('recharge/detail', 'RechargeController@rechargeDetail');              // 创建充值订单
    });
});