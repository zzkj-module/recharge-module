<?php
namespace Modules\Recharge\Services\Admin;

use Modules\Recharge\Validate\Common\IDValidate;
use Illuminate\Support\Facades\Cookie;

class BaseService
{
    /**
     * 每页显示多少条
     *
     * @return array|int|string|null
     */
    public function perPage()
    {
        $perPage = Cookie::get('admin_per_page') ?? 10;

        return $perPage < 100 ? $perPage : 100;
    }

    /**
     * 获取ID参数
     * @param $request
     * @return mixed
     * @throws \Exception
     * @author tan bing
     * @date 2021-06-28 11:22
     */
    public function getRequestID($request)
    {
        $id = $request->input('id', '');
        $IDValidate = new IDValidate();
        if(!$IDValidate->check(compact('id'))) {
            throw new \Exception($IDValidate->getError());
        }
        return $id;
    }

    /**
     * @param $query
     * @return array
     * @author tan bing
     * @date 2021-07-20 17:14
     */
    public function returnPageData($query)
    {
        $count = $query->total();
        $data  = $query->items();
        return compact('count', 'data');
    }

    /**
     * 获取请求参数
     * @return mixed
     * @author tan bing
     * @date 2021-07-20 17:33
     */
    public function getRequestParam()
    {
        return request()->input();
    }
}