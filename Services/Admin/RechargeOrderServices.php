<?php
/**
 *RechargeOrderServices
 * @author tan bing
 * @date 2021-09-17 11:46
 */


namespace Modules\Recharge\Services\Admin;


use Illuminate\Http\Request;
use Modules\Recharge\Repositories\Contracts\RechargeOrderInterface;

class RechargeOrderServices extends BaseService
{

    /**
     * @var Request
     * @author tan bing
     */
    private $request;

    /**
     * @var RechargeOrderInterface
     * @author tan bing
     */
    private $rechargeOrderInterface;

    /**
     * RechargeOrderServices constructor.
     * @param Request $request
     * @param RechargeOrderInterface $rechargeOrderInterface
     * @author tan bing
     * @date 2021-09-17 14:48
     */
    public function __construct(Request $request, RechargeOrderInterface $rechargeOrderInterface)
    {
        $this->request = $request;
        $this->rechargeOrderInterface = $rechargeOrderInterface;
    }

    /**
     * 获取详情
     * @param null $id
     * @return mixed
     * @author tan bing
     * @date 2021-09-17 14:48
     */
    public function getDetail($id = null)
    {
        !$id && $id = $this->request->input('id');
        return $this->rechargeOrderInterface->findById($id);
    }

    /**
     * 获取列表 --分页
     * @return array
     * @author tan bing
     * @date 2021-09-17 14:50
     */
    public function getPageData()
    {
        $param = $this->request->input();
        $query = $this->rechargeOrderInterface->getPageData($param, $this->perPage());
        $count = $query->total();
        $data = $query->items();

        return compact('count', 'data');
    }
}