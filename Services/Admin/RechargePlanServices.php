<?php
/**
 *CouponServices
 * @author tan bing
 * @date 2021-09-15 17:19
 */


namespace Modules\Recharge\Services\Admin;


use Illuminate\Http\Request;
use Modules\Recharge\Entities\Coupon;
use Modules\Recharge\Entities\RechargePlan;
use Modules\Recharge\Validate\Admin\RechargeValidate;
use Modules\Recharge\Repositories\Contracts\RechargePlanInterface;

class RechargePlanServices extends BaseService
{

    /**
     * @var Request
     * @author tan bing
     */
    private $request;

    /**
     * @var RechargeValidate
     * @author tan bing
     */
    private $validate;

    /**
     * @var RechargePlanInterface
     * @author tan bing
     */
    private $rechargeInterface;

    /**
     * RechargeServices constructor.
     * @param Request $request
     * @param RechargeValidate $rechargeValidate
     * @param RechargePlanInterface $rechargeInterface
     * @author tan bing
     * @date 2021-09-15 17:24
     */
    public function __construct(Request $request, RechargeValidate $rechargeValidate, RechargePlanInterface $rechargeInterface)
    {
        $this->request = $request;
        $this->validate = $rechargeValidate;
        $this->rechargeInterface = $rechargeInterface;
    }

    /**
     * 获取详情
     * @param string $id
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:27
     */
    public function getRechargeDetail($id = '')
    {
        !$id && $id = $this->request->input('id');
        return $this->rechargeInterface->findById($id);
    }

    /**
     * 获取列表  --分页
     * @return array
     * @author tan bing
     * @date 2021-09-15 17:29
     */
    public function getPageData()
    {
        $param = $this->request->input();
        $query = $this->rechargeInterface->getPageData($param, $this->perPage());
        $count = $query->total();
        $data = $query->items();

        return compact('count', 'data');
    }

    /**
     * 获取列表
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:49
     */
    public function getList()
    {
        $param = $this->request->input();
        return $this->rechargeInterface->getList($param);
    }

    /**
     * 添加或编辑
     * @return mixed
     * @throws \Exception
     * @author tan bing
     * @date 2021-09-15 17:49
     */
    public function addAndEditDetail()
    {
        $data = $this->request->post();
        $scene = !empty($data['id']) ? 'edit' : 'add';
        if(!$this->validate->scene($scene)->check($data)) {
            throw new \Exception($this->validate->getError());
        }
        try {
            $result = $this->rechargeInterface->addAndEditDetail($data);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * 修改状态
     * @return mixed
     * @throws \Exception
     * @author tan bing
     * @date 2021-09-15 17:51
     */
    public function editStatus()
    {
        $param = $this->request->post();
        if (!$this->validate->scene('status')->check($param)) {
            throw new \Exception($this->validate->getError());
        }
        $model = new RechargePlan();
        if (!$result = $model->editStatus($param)) throw new \Exception('操作失败');

        return $result;
    }

    /**
     * 删除
     * @return mixed
     * @author tan bing
     * @date 2021-09-15 17:51
     */
    public function delete()
    {
        $id     = $this->request->get('id', '');
        $model = new RechargePlan();
        return $model->forceDeleteData($id);
    }

    /**
     * 批量删除
     * @return mixed
     * @author tan bing
     * @date 2021-06-24 10:00
     */
    public function batchDelete()
    {
        $ids    = $this->request->get('ids', '');
        $model = new RechargePlan();
        return $model->batchForceDeleteData($ids);
    }

    /**
     * 获取优惠券列表
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author tan bing
     * @date 2021-09-17 10:33
     */
    public function getCouponList()
    {
        $id = $this->request->input('id');
        $couponIds = [];
        $couponList = Coupon::query()->where('status', 1)->select('id as value', 'coupon_name as name')->get();
        if($id) {
            $rechargePlan = $this->rechargeInterface->findById($id);
            $couponIds = explode(',', $rechargePlan->gift_coupons);
        }
        foreach ($couponList as $coupon => $value) {
            foreach ($couponIds as $couponId) {
                if($value->value == $couponId) {
                    $couponList[$coupon]['selected'] = true;
                }
            }
        }

        return $couponList;
    }
}