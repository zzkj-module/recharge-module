<?php
/**
 *CouponValidate
 * @author tan bing
 * @date 2021-09-15 17:22
 */


namespace Modules\Recharge\Validate\Admin;


use Modules\Recharge\Validate\BaseValidate;

class RechargeValidate extends BaseValidate
{

    protected $rule = [];

    protected $message = [];

    protected $scene = [
        'add' => [],
        'edit' => [],
        'status' => [],
    ];
}