<?php
/**
 *IDValidate
 * @author tan bing
 * @date 2021-06-28 11:16
 */

namespace Modules\Recharge\Validate\Api;

use Modules\Recharge\Validate\BaseValidate;

class RechargeValidate extends BaseValidate
{
    protected $rule = [
        'amount' => 'nullable|numeric|min:1',
        'plan_id' => 'nullable|integer',
    ];

}