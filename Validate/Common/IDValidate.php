<?php
/**
 *IDValidate
 * @author tan bing
 * @date 2021-06-28 11:16
 */

namespace Modules\Recharge\Validate\Common;

use Modules\Recharge\Validate\BaseValidate;

class IDValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'required|integer'
    ];

}