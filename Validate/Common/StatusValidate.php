<?php
/**
 *IDValidate
 * @author tan bing
 * @date 2021-06-28 11:16
 */


namespace Modules\Recharge\Validate\Common;


use Modules\Recharge\Validate\BaseValidate;

class StatusValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'required|integer',
        'status' => 'required|min:0,1',
    ];

    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID必须为正整数',
        'status.required' => '状态值不能为空',
        'status.in' => '状态值错误',
    ];

}